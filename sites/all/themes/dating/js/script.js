/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {


        $(window).scroll(function(){

          if (inView($('.s-circle'))) {
            $('.s-circle__count').once(function () {
              var $this = $(this);
              jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                duration: 2000,
                easing: 'swing',
                step: function () {
                  $this.text(Math.ceil(this.Counter));
                }
              });
            });
          }

          if ($(this).scrollTop() >= 500) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
          } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
          }

        });

      function inView(elem)
      {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
      }

      $('#return-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
          scrollTop : 0                       // Scroll to top of body
        }, 500);
      });

      $('.tos__link').click(function () {
        var $something= $('<input/>').attr({ class: 'tos__accept', type: 'button', name:'btn1', value:'Accept'});
          $("#cboxWrapper").append($something);
      });
      $('.tos__accept').click(function () {
        parent.jQuery.colorbox.close();
        $('.tos__check').prop("checked", true);
        return false;
      });

      if($(window).width()>1200){
        $('.s-social').insertAfter('.region-footer .menu__item:nth-child(3)');
      }

      // $(function() {
      //   $('a[href*="#"]:not([href="#"])').click(function() {
      //     if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      //       var target = $(this.hash);
      //       target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      //       if (target.length) {
      //         $('html, body').animate({
      //           scrollTop: target.offset().top
      //         }, 500);
      //         return false;
      //       }
      //     }
      //   });
      // });


    }
  };

  // Drupal.behaviors.fixHeader = {
  //   attach: function (context, settings) {
  //     $(document).scroll(function () {
  //       var scroll = $(window).scrollTop();
  //       if (scroll > $(".header").height()) {
  //         $('.region-navigation').addClass('fix');
  //         $(document.body).css("margin-top", $(".header").height()).resize();
  //         $('.s-promo').css("padding-top", 120).resize();
  //       }
  //       else {
  //         $('.region-navigation').removeClass('fix');
  //         $(document.body).css("margin-top", 0).resize();
  //         $('.s-promo').css("padding-top", 70).resize();
  //       }
  //     });
  //
  //   }
  // };

})(jQuery, Drupal, this, this.document);

