(function ($) {
    $(document).ready(function () {


        $("#block-menu-menu-front-menu .menu__link").click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                || location.hostname == this.hostname) {

                var target = $(this.hash),
                    headerHeight = $(".header-wrap").height() + 5; // Get fixed header height

                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - headerHeight
                    }, 1000);
                    return false;
                }
            }
        });

        var controller = new ScrollMagic.Controller();

        var ourHiw = new ScrollMagic.Scene({
            triggerElement: '#hiw',
            duration: '93%',
            triggerHook: 0.11
        }).setClassToggle('.hiw-link', 'scroll-active').addTo(controller);

        var ourFeatures = new ScrollMagic.Scene({
            triggerElement: '#features',
            duration: '98%',
            triggerHook: 0.15
        }).setClassToggle('.features-link', 'scroll-active').addTo(controller);

        var ourSuccesses = new ScrollMagic.Scene({
            triggerElement: '#successes',
            duration: '93%',
            triggerHook: 0.15
        }).setClassToggle('.successes-link', 'scroll-active').addTo(controller);


        $(".user__picture a img").unwrap();
        $("#block-menu-menu-front-menu #home").removeAttr('href');

        function ajaxGeneral() {

            $("header h2 a").removeAttr('href');

            $(".privatemsg-view-pager").hide();

            $(".messages-list, .lisr-dialogs").mCustomScrollbar({
                theme: "dark"
            });

            $(".messages-list").mCustomScrollbar("scrollTo", "bottom", {scrollInertia: 0});

            $("#communications-form .form-type-textarea label").click(function () {
                $("#communications-form form .field-widget-image-image.form-wrapper [type='file']").trigger('click');
            });

            $(".ajax-progress-throbber").hide();
            $(".ajax-progress").hide().css('display', 'none');

            $(".clear-filters").click(function () {
                $(this).siblings(".form-type-select").find('.form-type-bef-checkbox').removeClass('active');
                $(this).siblings(".form-type-select").find('label').removeClass('active');
                $(this).siblings(".form-type-select").find('input').prop("checked", false);
                $("#views-exposed-form-amusements-page [type='submit']").trigger("click");
            });

            $("#views-exposed-form-amusements-page .views-widget-filter-field_amusement_park_tid .bef-toggle").click(function () {
                $(this).toggleClass('active');
            });

            $("#views-exposed-form-amusements-page [type='checkbox']").each(function () {
                if ($(this).prop('checked')) {
                    $(this).siblings().toggleClass('active');
                }
            });

            $(".view-amusements .views-widget-filter-type_1 > label").click(function () {
                $(this).parent().toggleClass('active');
            });

            $(".view-amusements .views-widget-filter-type_1 .bef-checkboxes label, .view-amusements .views-widget-filter-type_1 .bef-toggle").click(function () {
                $(this).toggleClass('active');
            });

            $("#colorbox .form-type-radio input").each(function () {
                if ($(this).prop('checked')) {
                    $(this).parent().addClass('active');
                }
            });

            $("#colorbox .form-type-radio label").click(function () {

                $(this).parent().toggleClass('active');

                if ($(this).parent().siblings().hasClass('active')) {
                    $(this).parent().siblings().removeClass('active');
                }

            });

            $(".bef-tree-child .form-type-bef-checkbox label").click(function () {
                $(this).parent().toggleClass("active");
            });

            $(".view-filters .title-filter").click(function () {
                $(this).toggleClass('active');
                $(".view-filters #edit-field-amusement-park-tid-wrapper").toggle();
            });

            $("#add-func-comment-actions-form textarea").attr('placeholder', 'Your review');
            $("#add-func-comment-actions-form [type='submit']").attr('value', 'Add review');
        }

        ajaxGeneral();

        $(document).ajaxComplete(
            function () {
                ajaxGeneral();
            }
        );

        $(".view-amusements .button a.colorbox-load").colorbox({
            width: "480px",
            height: "310px",
            onOpen: openCallBack
        });

        function openCallBack() {


        }

    });
})(jQuery);
