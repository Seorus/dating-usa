<a href="#" id="return-to-top"></a>

<div class="page" id="fullpage">
  <?php include path_to_theme() . '/templates/inc/header.tpl.inc'; ?>
    <div class="top-banner" id="reg">
      <?php print drupal_render($register_form); ?>
    </div>
  <?php print $messages; ?>
  <?php print render($page['help']); ?>

  <?php
  $data = file_get_contents(path_to_theme() . '/content/content.json');
  $json = json_decode($data, TRUE);
  // echo '<pre>' . print_r($json, true) . '</pre>';
  ?>

  <?php foreach ($json['promo'] as $key => $value): ?>
      <div class="tracked s-promo s-promo--<?php echo $key; ?>" id="<?php echo $key; ?>">
          <div class="s-promo__inner page-width">
              <h2 class="s-promo__title"><?php echo $json['promo'][$key]['title']; ?></h2>
              <div class="s-promo__content">
                <?php foreach ($json['promo'][$key]['content'] as $value): ?>
                    <div class="s-promo__element">
                      <?php if ($key != 'features' && isset($value['title'])): ?>
                          <h3 class="s-promo__element-title"><?php echo $value['title']; ?></h3>
                      <?php endif; ?>
                        <img class="s-promo__element-image"
                             src="<?php echo path_to_theme() . '/css/img/' . $value['image']; ?>"
                             alt="">
                      <?php if ($key == 'features' && isset($value['title'])): ?>
                          <h3 class="s-promo__element-title"><?php echo $value['title']; ?></h3>
                      <?php endif; ?>
                        <div class="s-promo__element-text"><?php echo $value['text']; ?> </div>
                    </div>
                <?php endforeach; ?>
              </div>
            <?php if (isset($json['promo'][$key]['button-link']) && isset($json['promo'][$key]['button-link'])): ?>
                <a class="s-promo__button s-button" href="<?php
                echo $json['promo'][$key]['button-link']; ?>"><?php echo $json['promo'][$key]['button-text'];
                  ?></a>
            <?php endif; ?>
          </div>
      </div>
  <?php endforeach; ?>

    <div class="s-circle">
        <div class="s-circle__inner page-width">
            <div class="s-circle__content">
              <?php foreach ($json['circle']['content'] as $key => $value): ?>
                  <div class="s-circle__element s-circle__element--<?php echo $key; ?>">
                      <div class="s-circle__count s-circle__text"><?php echo $value['count']; ?></div>
                      <div class="s-circle__label s-circle__text"><?php echo $value['label']; ?></div>
                  </div>
              <?php endforeach; ?>
            </div>
            <a class="s-promo__button s-button" href="<?php
            echo $json['circle']['button-link']; ?>"><?php echo $json['circle']['button-text'];
              ?></a>
        </div>
    </div>
  <?php include path_to_theme() . '/templates/inc/footer.tpl.inc'; ?>
</div>