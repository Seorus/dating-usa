<div class="page">

    <?php include path_to_theme() . '/templates/inc/header.tpl.inc'; ?>

    <section class="main-wrap">

    <main class="communications-main-wrap" role="main">
        <?php print $messages; ?>
        <?php print render($page['highlighted']); ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

        <section class="communications-wrap">

            <div class="communications-users col">

                <?php $started = 0;
                if (isset($page['content']['system_main']['replystarted']['#value'])) $started = $page['content']['system_main']['replystarted']['#value'];
                print render($page['content']); ?>

            </div>

            <div class="communications-form col" id="communications-form" <?php if (module_exists('add_func') && $started > 0) {echo 'actual="'.$started.'"';} ?>>
                <?php if (module_exists('add_func') && $started > 0) {
                    $out = add_func_communications_get_form_reply($started, 2);
                    $out['participants']['#prefix']='<div class="messages-list">';
                    $out['messages']['#suffix']='</div>';
                    dpm($out);
                    echo render($out);
                } ?>
            </div>

        </section>

    </main>

    </section>
    <?php include path_to_theme() . '/templates/inc/footer.tpl.inc'; ?>

</div>