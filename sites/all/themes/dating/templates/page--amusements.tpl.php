<div class="page">
  <?php include path_to_theme() . '/templates/inc/header.tpl.inc'; ?>

    <main class="page-width" role="main">
        <?php print $messages; ?>
        <?php print render($page['highlighted']); ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </main>
  <?php include path_to_theme() . '/templates/inc/footer.tpl.inc'; ?>
</div>
