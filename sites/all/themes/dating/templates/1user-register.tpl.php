<h2 class="register__title">Join Now for Free</h2>
<?php
/**
* Prints Profile2 fields.
*/

print render($form['profile_main']['field_first_name']);



/**
* Prints Registration fields.
*/
//dpm($form['account']);

unset($form['account']['name']['#description']);
unset($form['account']['pass']['#description']);
unset($form['account']['mail']['#description']);
unset($form['account']['name']['#title']);
unset($form['account']['mail']['#title']);

$form['account']['name']['#attributes']['placeholder'] = t('Name');
$form['account']['mail']['#attributes']['placeholder'] = t('E-mail');
$form['actions']['submit']['#value'] = t('Sign Up');

$form["account"]["pass"] = array(
  "#type" => "password"
,"#attributes" => array(
        "placeholder" => 'Password'
  )
,"#disabled" => 0
,"#description" => null
) ;
$form["account"]["tos"] = array(
  "#type" => "checkbox"
,"#title" => "I certify that I am over 18 and have read and accepted the <a class=\"colorbox-node tos__link\" href=\"/tos\">Terms of Service</a>"
,"#attributes" => array("class" => array("tos__check"))
,"#default_value" => 1
,"#disabled" => 0
,"#description" => null
) ;

print render($form['account']['name']);
print render($form['account']['mail']);
print render($form['account']['pass']);
print render($form['account']['tos']);

//print render($form['account']['conf-mail']);
//print render($form['account']['pass-pass2']);
//print render($form['account']['regcode-code']);
//print render($form['account']['submit']);


print drupal_render($form['actions']);
print'<button class="s-button s-button--fb" formaction="user/simple-fb-connect">Facebook Login</button>';
//print drupal_render_children($form);