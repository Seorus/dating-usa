<div class="footer-wrap">
<?php drupal_add_js(path_to_theme() . '/js/script.js'); ?>
<?php print render($page['footer']); ?>

<div class="footer-bottom">
    <div class="copyright">
      <?php
      $block = module_invoke('block', 'block_view', '1');
      print render($block['content']);
      ?>

      <?php print "© " . date('Y') . ' ' . $site_name; ?>


    </div>
</div>

</div>