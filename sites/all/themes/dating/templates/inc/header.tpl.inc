<div class="header-wrap">
    <header class="header page-width" role="banner">
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>"
               title="<?php print t('Home'); ?>" rel="home"
               class="header__logo"><img src="<?php print $logo; ?>"
                                         alt="<?php print t('Home'); ?>"
                                         class="header__logo-image"/></a>
        <?php endif; ?>
        <?php if ($site_name): ?>
            <h1 class="header__site-name">
                <a href="<?php print $front_page; ?>"
                   title="<?php print t('Home'); ?>" class="header__site-link"
                   rel="home"><?php print $site_name; ?></a>
            </h1>
        <?php endif; ?>
        <?php print render($page['header']); ?>
    </header>
    <?php print render($page['navigation']); ?>
</div>
