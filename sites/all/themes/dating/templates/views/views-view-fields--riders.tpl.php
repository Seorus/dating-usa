<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
//dpm($fields);
?>
<div class="wrapper">
  <div class="images"><?php print ($fields['picture']->content); ?></div>
  <div class="content-wrapper">
    <div class="content-header">
      <strong>
        <?php print ($row->users_name); ?>
      </strong>
      (<?php print ($fields['field_user_firstname_1']->content . ' ' . $fields['field_user_lastname']->content); ?>)
      <ul>
        <li><?php //print($fields['ops_1']->content); ?></li>
        <li><?php print($fields['ops']->content); ?></li>
        <li class="speech"><?php print($fields['php']->content); ?></li>
      </ul>
    </div>
    <div class="content-about"><?php //print($fields['field_user_about']->content); ?></div>
    <div class="content-gender"><span>Looking for:
</span><?php print($fields['field_user_genderlook_1']->content); ?></div>
  </div>
</div>
