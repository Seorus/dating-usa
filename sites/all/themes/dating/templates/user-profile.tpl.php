<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>

<?php


//dpm($profile_fm);
//dpm($user_profile);
//dpm($elements);


?>

<div class="user"<?php print $attributes; ?>>
    <div class="section section--even">
        <div class="section__content user__base page-width">
          <?php print render($user_profile['user_picture']); ?>
            <div class="user__base-info">
                <h1 class="user__name"><?php print
                    render($user_profile['field_user_firstname'][0]) . " " .
                    render($user_profile['field_user_middlename'][0]) . " " .
                    render($user_profile['field_user_lastname'][0]) . " (" . $elements['#account']->name . ")";
                  ?></h1>

                <?php if (isset($user_profile['field_user_birthday'])): ?>
                <div class="user__field user__field--birthday">
                   <?php print render($user_profile['field_user_birthday']); ?>
                </div>
                <?php endif; ?>

                <div class="user__field user__field--mail">
                    <span class="user__field-label">E-mail: </span>
                    <span class="user__field-value"><?php print $elements['#account']->mail; ?></span>
                </div>





              <?php print render($user_profile['field_user_gender']); ?>

              <?php print render($user_profile['field_user_genderlook']); ?>

            </div>
          <?php print render($user_profile['links']); ?>


          <?php
          $roles = $elements['#account']->roles;
          $premium = "Premium Membership";
          $status = (in_array($premium, $roles))? "Premium Membership (without Ads)" : "Free Membership (with Ads)";

            ?>
<div class="profile__label">
<?php print $status; ?>
</div>

        </div>
    </div>
    <div class="section section--odd">
        <div class="section__content user__about-content page-width">
          <?php print render($user_profile['field_user_about']); ?>
        </div>
    </div>

    <div class="section section--even">
        <div class="section__content user__pref-content page-width">
          <?php print render($user_profile['field_user_preferences']); ?>
        </div>
    </div>




  <?php


  //print end($elements['#account']->roles);

  ?>

  <?php // print render($user_profile); ?>

</div>


