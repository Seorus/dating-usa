<?php


function dating_form_alter(&$form, &$form_state, $form_id){

    if (in_array($form_id, array('user_login', 'user_login_block'))) {
        $form['name']['#attributes']['placeholder'] = t('Username *');
        $form['pass']['#attributes']['placeholder'] = t('Password *');
    }

    if (TRUE === in_array($form_id, array('user_register_form'))) {
        $form['account']['name']['#attributes']['placeholder'] = t('Введите логин...');
        $form['account']['mail']['#attributes']['placeholder'] = t('Введите e-mail...');
    }

    if (TRUE === in_array($form_id, array('user_pass'))) {
        $form['name']['#attributes']['placeholder'] = t('Username or e-mail address *');
    }
}

function dating_menu_link($variables) {

    //get path alias of current page
    $current_path = drupal_get_path_alias();

    //get path alias of menu item
    $menu_path = drupal_get_path_alias($variables['element']['#href']);

    //if the href of the menu item is found in the current path
    if (strstr($current_path, $menu_path)){
        //add active-trail class to li and a tags for that item
        $variables['element']['#attributes']['class'][] = 'active-trail';
        $variables['element']['#localized_options']['attributes']['class'][] = 'active-trail';
    }

    return theme_menu_link($variables);
}

function dating_page_alter(&$page){
    drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js');
    drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js');
    drupal_add_js('https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.js');

    drupal_add_css('https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.4/jquery.fullpage.min.css');
}

/**
 * Override or insert variables into the html templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function dating_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  $variables['classes_array'] = array_diff($variables['classes_array'],
    array('class-to-remove')
  );
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("page" in this case.)
 */

function dating_preprocess_page(&$variables, $hook) {

  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function dating_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--no-wrapper.tpl.php template for sidebars.
  if (strpos($variables['region'], 'sidebar_') === 0) {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('region__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function dating_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  if ($variables['block_html_id'] == 'block-system-main') {
    $variables['theme_hook_suggestions'] = array_diff(
      $variables['theme_hook_suggestions'], array('block__no_wrapper')
    );
  }
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function dating_preprocess_node(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // Optionally, run node-type-specific preprocess functions, like
  // dating_preprocess_node_page() or dating_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}
// */

/**
 * Override or insert variables into the comment templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function dating_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

//function dating_theme(&$existing, $type, $theme, $path){
//  $hooks = array();
//
//  $hooks['user_register_form'] = array (
//    'render element' => 'form',
//    'path' => drupal_get_path('theme','dating'),
//    'template' => 'templates/user-register',
//    'preprocess functions' => array('dating_preprocess_user_register_form'),
//  );
//  $hooks['user_profile_form'] = array (
//    'arguments' => array('form' => NULL),
//    'render element' => 'form',
//    'template' => 'templates/user-profile-edit',
//  );
//
//  return $hooks;
//}
//
//function dating_preprocess_user_register_form(&$vars) {
//  $args = func_get_args();
//  array_shift($args);
//  $form_state['build_info']['args'] = $args;
//  $vars['form'] = drupal_build_form('user_register_form', $form_state['build_info']['args']);
//}
//
//function dating_user_register_form_submit($form, &$form_state){
//  dpm(1);
//  $edit = array(
//    'name' => $form_state['values']['name'],
//    'pass' => user_password(),
//    'mail' => $form_state['values']['mail'],
//    'init' => $form_state['values']['mail'],
//    'status' => 1,
//    'access' => REQUEST_TIME,
//  );
//  user_save(drupal_anonymous_user(), $edit);
//}
//function dating_get_user_register() {
//  $form = drupal_get_form('user_register_form');
//  return theme_status_messages().$form;
//}