<h2 class="register__title">Join Now for Free</h2>

<?php

unset($form['account']['name']['#description']);
unset($form['account']['pass']['#description']);
unset($form['account']['mail']['#description']);
unset($form['account']['pass']['#title']);
unset($form['account']['name']['#title']);
unset($form['account']['mail']['#title']);

$form['account']['name']['#attributes']['placeholder'] = t('Name');
$form['account']['pass']['#attributes']['placeholder'] = t('Password');
$form['account']['mail']['#attributes']['placeholder'] = t('E-mail');
$form['actions']['submit']['#value'] = t('Sign Up');



print drupal_render_children($form);
print'<button class="s-button s-button--fb" formaction="user/simple-fb-connect">Facebook Login</button>';

?>



