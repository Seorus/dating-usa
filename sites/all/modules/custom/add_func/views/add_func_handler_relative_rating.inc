<?php

/**
 * @file
 * Definition of add_func_handler_relative_rating.
 */

/**
 * Description of what my handler does.
 */
class add_func_handler_relative_rating extends views_handler_field {
    /**
     * Add some required fields needed on render().
     */
    function construct() {
        parent::construct();
        $this->additional_fields['field_relativerating'] = array(
            'table' => 'field_data_field_relativerating',
            'field' => 'field_relativerating_value',
        );
    }

    /**
     * Loads additional fields.
     */
    function query() {
        $this->ensure_my_table();
        $this->add_additional_fields();

    }

    /**
     * Renders the field handler.
     */
    function render($values) {

        $counts= db_query(add_func_create_query4relative_rating($values->uid,1))->fetchField()+
            db_query(add_func_create_query4relative_rating($values->uid,0))->fetchField()+
            db_query(add_func_create_query4relative_rating($values->uid,-1))->fetchField();

        return $counts;
    }
}