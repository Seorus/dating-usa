<?php


function add_func_views_data() {
    $data = array();
    $data['users']['relative_rating'] = array(
        'title' => t('Relative rating'),
        'help' => t('Custim field on Relative rating'),
        'field' => array(
            'handler' => 'add_func_handler_relative_rating',
            'click sortable' => TRUE,
        ),

        'sort' => array(
            'handler' => 'add_func_handler_sort_relative_rating',
        ),

    );
    return $data;
}

function add_func_create_query4relative_rating($uid,$val)
{
    global $user;
    return "SELECT COUNT(*)
FROM
node AS nd
INNER JOIN field_data_field_comment_action AS act ON act.entity_id = nd.nid
INNER JOIN field_data_field_target AS trg ON trg.entity_id = nd.nid
WHERE
nd.type = 'comment' AND
nd.uid = $uid AND
act.field_comment_action_value = $val
 AND
trg.field_target_target_id IN (SELECT
trg.field_target_target_id
FROM
node AS nd
INNER JOIN field_data_field_comment_action AS act ON act.entity_id = nd.nid
INNER JOIN field_data_field_target AS trg ON trg.entity_id = nd.nid
WHERE
nd.type = 'comment' AND
nd.uid = $user->uid AND act.field_comment_action_value=$val)";
}