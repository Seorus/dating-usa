<?php

/**
 * @file
 * Definition of views_handler_sort_random.
 */

/**
 * Handle a random sort.
 *
 * @ingroup views_sort_handlers
 */
/*
class views_handler_sort_random extends views_handler_sort {
    function query() {
        $this->query->add_orderby('handlername');
    }

    function options_form(&$form, &$form_state) {
        parent::options_form($form, $form_state);
        $form['order']['#access'] = FALSE;
    }
}

class mymodule_handler_sort_handlername extends views_handler_sort {
    /**
     * Overrides views_handler_sort::query().
     * /
    function query() {
        dpm($this);
        $this->ensure_my_table();
        // Add the field.
        $this->query->add_orderby(NULL, NULL,  $this->options['order'], $this->table . '_' . $this->field);
    }
}

<?php

/**
 * @file
 * Contains the flagged content sort handler.
 */

/**
 * Handler to sort on whether objects are flagged or not.
 *
 * @ingroup views
 */
class add_func_handler_sort_relative_rating extends views_handler_sort {


    function query() {
        $this->ensure_my_table();
        $this->query->add_orderby(NULL, "((".add_func_create_query4relative_rating("($this->table_alias.uid)",1).")+
        (".add_func_create_query4relative_rating("($this->table_alias.uid)",0).")+
        (".add_func_create_query4relative_rating("($this->table_alias.uid)",-1).")) ", $this->options['order'],'relative_rating');
    }
}
