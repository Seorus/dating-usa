<?php

function add_func_get_messages_list()
{
    global $user;
    return db_query("SELECT DISTINCT
users.`name`,
users.access,
file_managed.uri,
ind2.thread_id,
(SELECT
bm.body
FROM
pm_index AS bi
INNER JOIN pm_message AS bm ON bm.mid = bi.mid
WHERE
bi.thread_id = ind2.thread_id
ORDER BY
bi.mid DESC
LIMIT 0,1) as mess, 
(SELECT
Count(*)
FROM
pm_index AS pmi_new
WHERE
pmi_new.thread_id = ind2.thread_id AND 
pmi_new.is_new > 0 AND pmi_new.recipient=:uid) AS news,
(SELECT
Max(pmmm.`timestamp`)
FROM
pm_message AS pmmm
INNER JOIN pm_index AS pmim ON pmmm.mid = pmim.mid
WHERE
pmim.thread_id = ind2.thread_id) AS last 
FROM
pm_index AS ind2
INNER JOIN users ON users.uid = ind2.recipient
LEFT JOIN file_managed ON file_managed.fid = users.picture
WHERE ind2.thread_id IN (SELECT DISTINCTROW ind1.thread_id FROM pm_index ind1
                WHERE ind1.recipient = :uid AND ind1.deleted = 0) AND ind2.recipient <> :uid ORDER BY last DESC", array(':uid' => $user->uid))->fetchAll();
}

function add_func_privatemsg_link($uid)
{

    global $user;
    if(add_func_messages_get_not_ignored_me($user->uid, $uid)) {
        $thread_id = add_func_messages_get_thread_id($user->uid, $uid);
        if ($thread_id > 0)
            return l('Go to chat', 'messages/view/' . $thread_id, array('query' => drupal_get_destination()));
        else
            return l('Start a chat', 'messages/new/' . $uid, array('query' => drupal_get_destination()));
    }
}

function add_func_messages_get_thread_id($uid1, $uid2)
{
    return db_query("SELECT DISTINCT i1.thread_id FROM pm_index AS i1
                    INNER JOIN pm_index AS i2 ON i2.mid = i1.mid
                    WHERE i1.recipient = :uid1 AND i2.recipient = :uid2", array(':uid1' => $uid1, ':uid2' => $uid2))->fetchField();

}
function add_func_messages_get_not_ignored_me($uidmy, $uid)
{
    $count=db_select('flagging', 'f')
        ->condition('f.fid', 2)
        ->condition('f.entity_type', 'user')
        ->condition('f.entity_id', $uidmy)
        ->condition('f.uid', $uid)
        ->countQuery()
        ->execute()
        ->fetchField();
    if($count==0) return TRUE; else return FALSE;
}

function add_func_form_privatemsg_form_reply_alter(&$form, &$form_state, $form_id)
{
    $form['actions']['submit']['#value']='Send';
    form_load_include($form_state, 'inc', 'privatemsg', 'privatemsg.pages');
}
function add_func_form_privatemsg_new_alter(&$form, &$form_state, $form_id)
{
    global $user;
    $form['actions']['submit']['#value']='Send';
    $form['subject']['#value'] = 'chat ' . $user->uid;
    if (empty($form['recipient']['#default_value'])) {
        $recipient = $user;
        drupal_set_message('The recipient is not indicated - go to you');
        $form['recipient']['#default_value'] = $user->name;
    } else {
        $recipient = user_load_by_name($form['recipient']['#default_value']);
    }
    $form['subject']['#value'] = 'chat ' . $user->uid . ' to ' . $recipient->uid;
    $form['subject']['#type'] = 'hidden';
    $form['recipient']['#type'] = 'hidden';
    form_load_include($form_state, 'inc', 'privatemsg', 'privatemsg.pages');
}