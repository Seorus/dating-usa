<?php
function add_func_communications_form($form, &$form_state)
{

    $messages_list = add_func_get_messages_list();
    $form = array();
    if (count($messages_list) == 0) {
        $form['info'] = array('#markup' => 'No messages');
    } else {
        $in = $messages_list[0]->thread_id;
        if (arg(1) > 0) {
            foreach ($messages_list as $value) if (arg(1) == $value->thread_id) $in = $value->thread_id;
        }
        $reply = add_func_communications_get_form_reply($in, 1);
        $form['reply'] = array(
            '#type' => 'hidden',
            '#value' => serialize($reply),
        );
        $form['replystarted'] = array(
            '#type' => 'hidden',
            '#value' => $in,
        );
        $form['mess_fltr'] = array(
            '#title' => 'Filter By',
            '#type' => 'select',
            '#prefix'=>'<div class="filter-select"><div>',
            '#suffix'=>'</div></div>',
            '#default_value' => 0,
            '#options' => array(0 => 'all (Without archiving)', 1 => 'unread', 2 => 'arhived'),
        );
$first=1;
        foreach ($messages_list as $value) {
            $fltr = array();
            if ($value->news > 0) $fltr[] = array('value' => 1);
//  <img src="' .image_style_url('user_75',  (!empty($value->uri)?$value->uri:file_build_uri(variable_get('user_picture_default', '')))). '">
            $desc = '<div class="user-row '.($in==$value->thread_id?'active':'').'">
                    <div class="avatarka" number-messages="' . $value->thread_id . '">
                        
                        <img src="' .image_style_url('user_75',  (!empty($value->uri)?$value->uri:variable_get('user_picture_default', ''))). '">
                    </div>
                    <div class="info">
                        <div class="name-date">' .  $value->name . ' <span>' . date("Y/m/d H:i", $value->last) . '</span></div>
                        <div class="last-messages">' . add_func_communications_get_short_message($value->mess) .                '</div>
                    </div>
                </div>';
            if ((time() - $value->last) > 2592000)
                $fltr[] = array('value' => 2);
            else
                $fltr[] = array('value' => 0);
$messname='mess_' . $value->thread_id;
            $form[$messname] = array(
                '#type' => 'checkbox',
                '#title' => $desc,
                '#value' => $value->thread_id,
                '#states' => array(
                    'visible' => array(
                        ':input[name="mess_fltr"]' => $fltr,
                    ),
                ),

                '#ajax' => array(
                    'callback' => 'add_func_communications_load_data',
                    'wrapper' => 'communications-form',
                ),
            );
            if($first){  $form[$messname]['#prefix']='<div class="lisr-dialogs">'; $first=0;}
        }
        if(count($messages_list)>0) $form[$messname]['#suffix']='</div>';
        $form['#attached']['js'] = array(
            drupal_get_path('module', 'add_func') . '/add_func.js',
        );

    }
    return $form;
}

function add_func_communications_load_data($form, &$form_state)
{
    $reply = unserialize($form_state['values']['reply']);
    $mess = add_func_communications_get_form_reply($form_state['triggering_element']['#value'], 2);
    $mess['reply']['actions'] = $reply['actions'];
    $mess['reply']['#submit'] = $reply['#submit'];
    $mess['reply']['#validate'] = $reply['#validate'];
//    $mess['reply']['#action']=$reply['#action'];
    $mess['reply']['#action'] = '/communications/' . $form_state['triggering_element']['#value'];

    unset($mess['participants']);
    $mess['messages']['#prefix']='<div class="messages-list">';
    $mess['messages']['#suffix']='</div>';

    $communications = drupal_render($mess);
     return '<div class="communications-form col" id="communications-form" actual="'.$form_state['triggering_element']['#value'].'">' . $communications . '</div>';
}

function add_func_communications_get_form_reply($thread_id, $st)
{
    module_load_include('pages.inc', 'privatemsg');
    $thread = privatemsg_thread_load($thread_id);
    switch ($st) {
        case 1:
            $reply = drupal_get_form('privatemsg_form_reply', $thread);
            return $reply;
        case 2:
            return privatemsg_view($thread);
    }
}
function add_func_communications_get_short_message($message)
{
    $message=strip_tags($message);
    if(strlen($message)>50) $num_liter=strpos($message,' ',50); else $num_liter=50;
    if($num_liter>75) $num_liter=75;
    return substr($message,0,$num_liter);
}