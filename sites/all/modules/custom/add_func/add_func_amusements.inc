<?php
/*
<?php if(module_exists('add_func')) echo add_func_amusements_frm_comment($row->nid); ?>
 */
function add_func_amusements_frm_comment($nid)
{
    $frm=drupal_get_form('add_func_amusements_frm_comment_form',$nid);
    $context=drupal_render($frm);
    return add_func_amusements_frm_comment_wrap($context,$nid);
}
function add_func_amusements_frm_comment_wrap($context,$nid)
{
    return '<div  style="float: left; border: 1px dotted red; position: relative; display: inline-block; width: 100%;" id="change-comment-' . $nid . '">' . $context . '</div>';
}
function add_func_amusements_frm_comment_form($form, &$form_state,$nid){

    global $user;
    $comment = add_func_get_comment_on_nid($nid, $user->uid);
    $form['nid'] = array(
        '#type' => 'hidden',
        '#value' => $nid,
    );
    $form['nid_comment'] = array(
        '#type' => 'hidden',
        '#value' => 0,
    );
    $form['comment'] = array(
        '#title' => t('Comment'),
        '#type' => 'textarea',
    );
    $form['private'] = array(
        '#type' => 'checkbox',
        '#title' => t('Private comment'),
    );
    if(isset($comment['nid']))
    {
        $form['comment']['#default_value']=$comment['c_body'];
        $form['private']['#default_value']=$comment['c_private'];
        $form['nid_comment']['#value']=$comment['nid'];
    }
    $form['submit'] = array(
        '#type'  => 'submit',
        '#ajax' => array(
            'callback' => 'add_func_amusements_frm_comment_submit',
            'wrapper' => 'change-comment-'.$nid,
            'method' => 'replace',
            'effect' => 'fade',
        ),
        '#value' => 'Submit',
    );

    return $form;
}
function add_func_amusements_frm_comment_submit($form, $form_state) {
    // Dummy/dumb validation for demo purpose.
    if (!empty($form_state['input']['comment'])) {
        $form['#prefix'] ='<div class="message" style="color: red;">Done !</div>';
        $private=$form_state['input']['private']==1?1:0;
        if ($form_state['input']['nid_comment']>0)
            add_func_comment_actions_edit($form_state['input']['nid_comment'], NULL, $form_state['input']['comment'], $private);
        else
            add_func_comment_actions_create($form_state['input']['nid'], NULL, $form_state['input']['comment'], $private);
    }

    $out=drupal_render($form);
    return add_func_amusements_frm_comment_wrap($out,$form_state['input']['nid']);
}
// ===============================================================================================================================

function add_func_amusements_lnk_actions($nid)
{
    global $user;
    $comment = add_func_get_comment_on_nid($nid, $user->uid);
    $action=0; if(isset($comment['nid'])) $action = $comment['c_action'];
    return '<div id="change-regard-status-'.$nid.'">'.add_func_amusements_actions_html($action,$nid).'</div>';
}

function add_func_amusements_actions_html($action,$nid)
{
    $stl1=' style="border: 1px dotted green;    float: left;    margin: 5px;    padding: 5px;"';
    $stl2=' style="border: 2px dotted red;    float: left;    margin: 5px;    padding: 5px;"';

    $arr_def=array('<span'.$stl1.'>Dislike</span>','<span'.$stl1.'>Neutral</span>','<span'.$stl1.'>Like</span>');
    $arr_sel=array('<span'.$stl2.'>Dislike</span>','<span'.$stl2.'>Neutral</span>','<span'.$stl2.'>Like=</span>');
    $out= '<div class="lnk-actions">' .

        ($action==-1?$arr_sel[0]:l($arr_def[0], 'change-regard-status/' . $nid . '/dislike/nojs', array(
            'attributes' => array( 'class' => array('use-ajax'), 'title' => 'Dislike', ), 'html' => TRUE
        )) ).
        ($action==0?$arr_sel[1]:l($arr_def[1], 'change-regard-status/' . $nid . '/neutral/nojs', array(
            'attributes' => array( 'class' => array('use-ajax'), 'title' => 'Neutral', ), 'html' => TRUE
        )) ).
        ($action==1?$arr_sel[2]:l($arr_def[2], 'change-regard-status/' . $nid . '/like/nojs', array(
            'attributes' => array( 'class' => array('use-ajax'), 'title' => 'Like', ), 'html' => TRUE
        )) ).

        '</div>';
    return $out;
}

function change_regard_status_ajax_callback($nid, $regard, $mode = NULL) {
    if ($mode != 'ajax') {
        drupal_set_message('Enable Javascript');
        drupal_goto(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '<front>');
    }
    global $user;
    $comment = add_func_get_comment_on_nid($nid, $user->uid);
    $arr_regard=array( 'dislike'=> -1, 'neutral'=> 0, 'like'=> 1 );
    if(isset($comment['nid']))
    {
        add_func_comment_actions_edit($comment['nid'], $arr_regard[$regard]);
    }
    else
    {
        add_func_comment_actions_create($nid, $arr_regard[$regard]);
    }

    $commands[] = ajax_command_html('#change-regard-status-'.$nid, add_func_amusements_actions_html($arr_regard[$regard],$nid));

    return array(
        '#type' => 'ajax',
        '#commands' => $commands,
    );
}

// ===============================================================================================================================
function add_func_amusements_frm_media($nid)
{
    global $user;
    $comment = add_func_get_comment_on_nid($nid, $user->uid);
    if(isset($comment['nid']) and $comment['fids']>0)
    {
        $node=node_load($comment['nid']);
       // $category = field_view_field('node', $node, 'field_media', array('label'=>'hidden'));
        $category = field_view_field('node', $node, 'field_media', array('label'=>'hidden'));
        $out= drupal_render($category);
        return add_func_amusements_frm_media_wrap($out,$nid);
    }
    $frm=drupal_get_form('add_func_amusements_frm_media_form',$nid);
    $context= drupal_render($frm);

    return add_func_amusements_frm_media_wrap($context,$nid);
}

function add_func_amusements_frm_media_wrap($context,$nid)
{
    return '<div id="change-media-' . $nid . '" style="float: left; border: 1px dotted red; position: relative; display: inline-block; width: 100%;">' .
    $context . '</div>';
}

function add_func_amusements_frm_media_form($form, &$form_state,$nid){


    global $user;
    $comment = add_func_get_comment_on_nid($nid, $user->uid);
    $form['nid'] = array(
        '#type' => 'hidden',
        '#value' => $nid,
    );
    $form['nid_comment'] = array(
        '#type' => 'hidden',
        '#value' => 0,
    );
    if(isset($comment['nid']))
    {
        $form['nid_comment']['#value']=$comment['nid'];
    }
    $tmpform = array();
    $tmpform_state = array();
    $tmpnode = new stdClass();
    $tmpnode->type = 'comment';
    field_attach_form('node', $tmpnode, $tmpform, $tmpform_state, NULL, array(
        'field_name' => 'field_media'
    ));
    $form['field_media'] = $tmpform['field_media'];
    $form['field_media']['#weight']=0;

    $form['submit'] = array(
        '#type'  => 'submit',
        '#ajax' => array(
            'callback' => 'add_func_amusements_frm_media_submit',
            'wrapper' => 'change-media-'.$nid,
            'method' => 'replace',
            'effect' => 'fade',
        ),
        '#value' => 'Submit',
    );

    return $form;
}
function add_func_amusements_frm_media_submit($form, $form_state) {
    if (isset($form_state['input']['field_media']['und']) && count($form_state['input']['field_media']['und'])>0) {
        $form['#prefix'] ='<div class="message" style="color: red;">Done !</div>';
        if ($form_state['input']['nid_comment']>0)
            $node =add_func_comment_actions_edit($form_state['input']['nid_comment'], NULL, NULL, NULL,  $form_state['input']['field_media']['und']);
        else
            $node =add_func_comment_actions_create($form_state['input']['nid'], NULL, NULL, NULL,  $form_state['input']['field_media']['und']);
        $category = field_view_field('node', $node, 'field_media', array('label'=>'hidden'));
        $out= drupal_render($category);
    }
    else
    {
        $out=drupal_render($form);
    }
    return add_func_amusements_frm_media_wrap($out,$form_state['input']['nid']);
}
// ===============================================================================================================================
